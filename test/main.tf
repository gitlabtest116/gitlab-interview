locals {
required_tags = {
      "Application" = "gitlab"
      "Application ID" = "0000" 
      "Environment" = "test"
    }
tags = merge(var.default_tags, local.required_tags)
}



// Retrieve VPC ID
data "aws_vpc" "gitlab_vpc" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["vpc_name"]]
  }
}

data "aws_subnet" "dmz_subnet_1" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["dmz_subnet_1"]]
  }
}


data "aws_subnet" "dmz_subnet_2" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["dmz_subnet_2"]]
  }
}



// Retrieve subnet ID 
data "aws_subnet" "gitlab-gitlab-subnet-db-1" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["subnet_db_1"]]
  }
}

// Retrieve subnet ID
data "aws_subnet" "gitlab-gitlab-subnet-db-2" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["subnet_db_2"]]
  }
}

// Retrieve subnet ID
data "aws_subnet" "gitlab-gitlab-app-subnet-1" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["subnet_name_1"]]
  }
}

// Retrieve subnet ID
data "aws_subnet" "gitlab-gitlab-app-subnet-2" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["subnet_name_2"]]
  }
}



data "aws_security_group" "sg-gitlab-jmp-gitlab" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["sg_test_jmp_gitlab"]]
  }
}


data "aws_security_group" "sg-gitlab-web-gitlab" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["sg_test_web_gitlab"]]
  }
}

data "aws_security_group" "sg-gitlab-app-gitlab" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["sg_test_app_gitlab"]]
  }
}

data "aws_security_group" "sg-gitlab-lb-gitlab" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["sg_test_lb_gitlab"]]
  }
}

data "aws_security_group" "sg-gitlab-rds-gitlab" {
  filter {
    name   = "tag:Name"
    values = [var.network_details["sg_test_rds_gitlab"]]
  }
}



//Jumphost Module

module "jumphost" {
  source                        = "../modules/jumphost/"
  sg_description                = "This security group is used for gitlab site jump host"
  aws_region                    = var.aws_region
  az                            = var.az
  environment                   = var.environment
  vpc_name                      = data.aws_vpc.gitlab_vpc.id
  default_tags              = local.tags
 /* dmz_subnet_1                  = data.aws_subnet.dmz_subnet_1.id
  dmz_subnet_2                  = data.aws_subnet.dmz_subnet_2.id
  sg_terminal_server_name       = data.aws_security_group.sg-terminal-server.id
  
  sg_gitlab_rds_gitlab          = data.aws_security_group.sg-gitlab-rds-gitlab.id
  sg_gitlab_backend_gitlab       = data.aws_security_group.sg-gitlab-backend-gitlab.id
  sg_gitlab_frontend_gitlab     = data.aws_security_group.sg-gitlab-frontend-gitlab.id
  sg_gitlab_lb_gitlab           = data.aws_security_group.sg-gitlab-lb-gitlab.id
  instance_details_jmp          = var.instance_details_jmp
  nwk_gitlab_onpremise_cidrs = var.nwk_gitlab_onpremise_cidrs
  nwk_gitlab_cloud_cidrs     = var.nwk_gitlab_cloud_cidrs
  nwk_gitlab_vpn_cidr        = var.nwk_gitlab_vpn_cidr
  network_details               = var.network_details*/
}

//Load Balancing Module

module "gitlab-loadbalancing" {
  source                              = "../modules/load-balancing"
  sg_description                      = "This security group allows is for gitlab Load balancer"
  environment                         = var.environment
  aws_region                          = var.aws_region
  vpc_name                            = data.aws_vpc.gitlab_vpc.id
  //private_hosted_zones                = var.private_hosted_zones
 // sg_gitlab_frontend_gitlab           = data.aws_security_group.sg-gitlab-frontend-gitlab.id
  default_tags                    = local.tags
 /* vpc_subnets                         = [data.aws_subnet.dmz_subnet_1.id, data.aws_subnet.dmz_subnet_2.id]
  cert_domain                         = var.cert_domain
  alb_tg_type                         = var.alb_tg_type
  alb_tg_port                         = var.alb_tg_port
  alb_tg_protocol                     = var.alb_tg_protocol
  alb_tg_healthy_shreshold            = var.alb_tg_healthy_shreshold
  alb_tg_interval                     = var.alb_tg_interval
  alb_tg_connect_port                 = var.alb_tg_connect_port
  alb_tg_connect_protocol             = var.alb_tg_connect_protocol
  alb_tg_timeout                      = var.alb_tg_timeout
  alb_tg_unhealthy_threshold          = var.alb_tg_unhealthy_threshold
  alb_tg_path                         = var.alb_tg_path
  alb_tg_matcher                      = var.alb_tg_matcher
  lb_type                             = var.lb_type
  lb_internal                         = var.lb_internal
  lb_enable_cross_zone_load_balancing = var.lb_enable_cross_zone_load_balancing
  lb_enable_deletion_protection       = var.lb_enable_deletion_protection
  lb_ip_address_type                  = var.lb_ip_address_type
  lb_port                             = var.lb_port
  lb_protocol                         = var.lb_protocol
  lb_ssl_policy                       = var.lb_ssl_policy
  alb_web_tg_port                    = var.alb_web_tg_port
  alb_web_tg_protocol                = var.alb_web_tg_protocol
  alb_web_tg_connect_protocol        = var.alb_web_tg_connect_protocol
  alb_web_tg_path                    = var.alb_web_tg_path
  target_id                           = module.ec2.target_id
  sg_gitlab_webproof_gitlab          = data.aws_security_group.sg-gitlab-lysoproof-gitlab.id*/
}

module "ebskey" {
  source           = "../modules/kms"
  kms_description  = var.kms_description
  alias_name       = "alias/kms-${var.aws_region}-gitlab_ebs-${var.environment}"
  default_tags = local.tags
}


module "ec2-web" {
  source                        = "../modules/ec2-web"
  environment                   = var.environment
  sg_web_description           = var.sg_web_description
  aws_region                    = var.aws_region
  az                            = var.az
  vpc_name                      = data.aws_vpc.gitlab_vpc.id
  default_tags             = local.tags
 /* subnet_name_4                 = data.aws_subnet.gitlab-gitlab-app-subnet-4.id
  subnet_name_5                 = data.aws_subnet.gitlab-gitlab-app-subnet-5.id
  sg_terminal_server_name       = data.aws_security_group.sg-terminal-server.id
  sg_gitlab_lb_gitlab           = data.aws_security_group.sg-gitlab-lb-gitlab.id
  instance_details_web         = var.instance_details_web
  network_details               = var.network_details
  ebs_kms_key                   = module.ebskey.arn
  host_dns_record               = data.aws_route53_zone.phz_vpc_gitlab.zone_id*/
}


module "ec2-app" {
  source                        = "../modules/ec2-app"
  environment                   = var.environment
  sg_app_description           = var.sg_app_description
  aws_region                    = var.aws_region
 az                            = var.az
  vpc_name                      = data.aws_vpc.gitlab_vpc.id
  default_tags                 = local.tags
  subnet_name_1              = data.aws_subnet.gitlab-gitlab-app-subnet-1.id
  subnet_name_2               = data.aws_subnet.gitlab-gitlab-app-subnet-2.id
 
  
  instance_details_app          = var.instance_details_app
  network_details               = var.network_details
  ebs_kms_key                   = module.ebskey.arn
 
}