terraform {
  backend "s3" {
    bucket               = "gitlab-terraform-state-gitlab-308703592800"
    dynamodb_table       = "gitlab-gitlab-308703592800-terraform-lock-table"
    key                  = "statefiles/gitlab-308703592800/test"
    region               = "us-east-1"
  }
}
