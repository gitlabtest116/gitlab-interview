variable "aws_region" {
  type = string
}

variable "account_id_gitlab" {
  type = string
}

variable "default_tags" {
  type = map(any)
}

variable "az" {
  type    = string
  default = "us-east-1a"
}

variable "environment" {
  default = "test"
}



variable "instance_details_app" {
  type = map(any)

  default = {
    instance_type = "t3.large"
    ami_id        = "ami-026b57f3c383c2eec"
    
  }
}

variable "instance_details_jmp" {
  type = map(any)

  default = {
    instance_type = "t3.large"
    ami_id        = "ami-026b57f3c383c2eec"

  }
}


variable "instance_details_web" {
  type = map(any)

  default = {
    instance_type = "t3.large"
    ami_id        = "ami-026b57f3c383c2eec"

  }
}


variable "network_details" {
  type = map(any)
  default = {
    vpc_name      = "vpc-us-east-1-gitlab"
    dmz_subnet_1  = "sbn-us-east-1a-d-dmz"
    dmz_subnet_2  = "sbn-us-east-1b-d-dmz"
    subnet_name_1 = "sbn-us-east-1a-d-application"
    subnet_name_2 = "sbn-us-east-1b-d-application"
    subnet_name_3 = "sbn-us-east-1a-application2"
    subnet_name_4 = "sbn-us-east-1b-application2"
    subnet_db_1   = "sbn-us-east-1a-d-database"
    subnet_db_2   = "sbn-us-east-1b-d-database"
    sg_test_gitlab_alb       = "sgrp-test-gitlab-alb"

    sg_test_jmp_gitlab       = "sg-us-east-1-test-jmp-gitlab"
    sg_test_app_gitlab  = "sg-us-east-1-test-app-gitlab"
    sg_test_web_gitlab    = "sg-us-east-1-test-web-gitlab"
    sg_test_lb_gitlab        = "sg-us-east-1-test-alb-gitlab"
   
    sg_test_rds_gitlab       = "sg-us-east-1-test-rds-gitlab"
  
    az_1                       = "us-east-1b"
    dns_zone                   = "gitlab-vpc.aws.test.cloud"
  }
}

variable "sg_app_description" {
default = "Security group for APP servers"

}

variable "sg_web_description" {
default = "security group for web server"  
}
/*
variable "keypair_name" {
  default = "test-frontend-gitlab"
}

variable "private_hosted_zones" {
}

variable "test_gitlab_route53_fqdn" {
  default = "test-gitlab"
}


#RDS variables

variable "db_name" {
  default = "rds-us-east-1-testsite-gitlab"
}

variable "db_identifier" {
  default = "rds-us-east-1-testsite-gitlab"
}

variable "db_instance" {
 
  default = "db.t3.medium"
}

variable "db_engine" {
  default = "mysql"
}


variable "db_allocated_storage" {
  default = "200"
}

variable "db_admin" {
  default = "admin"
}

#ALB Variables
variable "alb_tg_type" {
  type    = string
  default = "ip"
}

variable "alb_tg_port" {
  type    = string
  default = "80"
}

variable "alb_tg_protocol" {
  type    = string
  default = "HTTP"
}

variable "alb_tg_healthy_shreshold" {
  type    = string
  default = "2"
}

variable "alb_tg_interval" {
  type    = string
  default = "300"
}

variable "alb_tg_connect_port" {
  type    = string
  default = "traffic-port"
}

variable "alb_tg_connect_protocol" {
  type    = string
  default = "HTTP"
}

variable "alb_tg_timeout" {
  type    = string
  default = "5"
}

variable "alb_tg_unhealthy_threshold" {
  type    = string
  default = "10"
}

variable "container_port" {
  default = "80"
}

variable "alb_tg_path" {
  type    = string
  default = "/"
}

variable "alb_tg_matcher" {
  type    = string
  default = "200-299"
}

variable "lb_type" {
  type    = string
  default = "application"
}

variable "lb_internal" {
  type    = string
  default = "false"
}

variable "lb_enable_cross_zone_load_balancing" {
  type    = string
  default = "true"
}

variable "lb_enable_deletion_protection" {
  type    = string
  default = "true"
}

variable "lb_ip_address_type" {
  type    = string
  default = "ipv4"
}

variable "lb_port" {
  type    = string
  default = "443"
}

variable "lb_protocol" {
  type    = string
  default = "HTTPS"
}

variable "lb_ssl_policy" {
  type    = string
  default = "ELBSecurityPolicy-TLS-1-2-2017-01"
}

#variable "zone_id" {
#  type    = string
#  default = ""
#}

variable "cert_domain" {
  type    = string
  default = ""
}
*/
variable "kms_description" {
  default = "KMS Key for EFS encryption"
}


