terraform {
  backend "s3" {
    bucket               = "gitlab-terraform-state-gitlab-308703592800"
    dynamodb_table       = "gitlab-gitlab-308703592800-terraform-lock-table"
    key                  = "gitlab"
    region               = "us-east-1"
  }
}
