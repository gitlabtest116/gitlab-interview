# creation of new application subnets
resource "aws_subnet" "private" {
  count                = length(var.private_subnets)
  vpc_id               = module.vpc.vpc_id
  cidr_block           = var.private_subnets[count.index]
  availability_zone    = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null

  tags = merge(
    {
      Name        = join("-", ["sbn", var.azs[count.index], var.private_subnet_suffix[count.index]])
      subnet_type = var.private_subnet_suffix[0]
      AZ          = var.azs[count.index]
    },
    var.default_tags,
  )
}

###########################
# route for private subnets
###########################

# creation of private route tables
resource "aws_route_table" "private" {
  count  = length(var.private_subnets) > 0 ? 1 : 0
  vpc_id = module.vpc.vpc_id

  tags = merge(
    {
      "Name"  = join("-", ["rt", var.name, var.private_subnet_route_suffix])
      rt_type = var.private_subnet_route_suffix
    },
    var.default_tags,
  )
}

###########################
# nat gateway creation
###########################

resource "aws_eip" "nat" {
  count = var.nat_gateway == true ? 1 : 0
  vpc   = true

tags = merge(
    {
      Name = join("-", ["eip", var.name])
    },
    var.default_tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "natgw" {
    count = var.nat_gateway == true ? 1 : 0
  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(module.vpc.public_subnets, count.index)

tags = merge(
    {
      Name = join("-", ["nat", var.name])
    },
    var.default_tags,
  )

  lifecycle {
    create_before_destroy = true
  }
}

## creation of routes to gitlab via nat gateway and transit gateway

resource "aws_route" "private_nat_gateway" {
  count                  = length(var.private_subnets) > 0 && var.nat_gateway == true ? 1 : 0
  route_table_id         = aws_route_table.private[0].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private-route_aws_accounts" {
  count                  = length(var.private_subnets) > 0 && var.nat_gateway == true ? length(var.aws_destination_cidr_blocks) : 0
  route_table_id         = aws_route_table.private[0].id
  destination_cidr_block = element(var.aws_destination_cidr_blocks, count.index)
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
}



resource "aws_vpc_endpoint_route_table_association" "private-route-s3-endpoint" {
  route_table_id  = aws_route_table.private[0].id
  vpc_endpoint_id = module.s3_endpoint.id
}

# private subnet route table association
resource "aws_route_table_association" "private" {
  count          = length(var.private_subnets) > 0 ? length(var.private_subnets) : 0
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private[0].id
}