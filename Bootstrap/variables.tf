// declare variables defined in global-landingzone.tfvars
variable "aws_region" {
  type = string
}

// declare variables defined in global-landingzone.tfvars
variable "account_id_gitlab" {
  type = string
}

variable "environment" {
  type    = string
  default = "gitlab"
}

/* VPC */
variable "cidr_block_vpc_gitlab" {
  default = "10.11.0.0/20"
}

/* Subnets */
variable "cidr_block_dmz_subnet_a" {
  default = "10.11.0.0/24"
}

variable "cidr_block_dmz_subnet_b" {
  default = "10.11.1.0/24"
}

variable "cidr_block_application_subnet_a" {
  default = "10.11.2.0/24"
}

variable "cidr_block_application_subnet_b" {
  default = "10.11.3.0/24"
}
variable "cidr_block_private_subnet_a" {
  default = "10.11.4.0/24"
}

variable "cidr_block_private_subnet_b" {
  default = "10.11.5.0/24"
}

variable "cidr_block_private_subnet_c" {
  default = "10.11.6.0/24"
}

/* VPC name */
variable "vpc_name_gitlab" {
  default = "gitlab"
}

/* Subnet AZs */
variable "subnet_az_a" {
  default = "us-east-1a"
}

variable "subnet_az_b" {
  default = "us-east-1b"
}

variable "subnet_az_c" {
  default = "us-east-1c"
}

/* Tags attributes for VPC gitlab */
variable "env_vpc_gitlab" {
  default = "d"
}

/* Tags attributes for Subnet DMZ gitlab */
variable "env_subnet_dmz_gitlab" {
  default = "d"
}

/* Tags attributes for Subnet Private gitlab */
variable "env_subnet_private_gitlab" {
  default = "d"
}

/* Tags attributes for Subnet Application gitlab */
variable "env_subnet_application_gitlab" {
  default = "d"
}



variable "shared_vpc_side_asn" {
  default = 65123
}


// declare variables defined in global-landingzone.tfvars
variable "default_tags" {
  type = map(any)
}

variable "nwk_routing_tables_name" {
  type = map(any)
  default = {
    "dmz"         = "rt-us-east-1-gitlab-dmz"
    "application" = "rt-us-east-1-gitlab-application"
    "database"    = "rt-us-east-1-gitlab-database"
  }
}

variable "nwk_default_route_cidr" {
  type    = string
  default = "0.0.0.0/0"
}

variable "private_subnet_route_suffix" {
  description = "Suffix to append to private route name"
  type        = string
  default     = "application2"
}

variable "private_subnet_suffix" {
  description = "Suffix to append to private subnets name"
  type        = list(string)
  default     = ["application2", "application2"]
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
  default     = ["10.11.11.0/24", "10.11.10.0/24"]
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b"]
}

variable "name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "us-east-1-gitlab"
}

variable "nat_gateway" {
  description = "Whether Nat Gateway is used or not"
  type        = bool
  default     = true
}


variable "aws_destination_cidr_blocks" {
  description = "The AWS destination CIDR block"
  type        = list(any)
  default     = ["10.6.0.0/20", "10.12.0.0/20", "10.13.0.0/20"]
}