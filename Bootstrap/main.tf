/* This layer defines the VPC to be deployed in gitlab */

module "vpc" {
  source = "../modules/vpc-community"

  // default tags for all VPC resources
  tags = merge(
    var.default_tags,
    {
      "Application" = "none"
      "Role"        = "network"
      "Environment" = var.environment
    }
  )

  name = "vpc-${var.aws_region}-${var.vpc_name_gitlab}"
  cidr = var.cidr_block_vpc_gitlab
  azs  = [var.subnet_az_a, var.subnet_az_b]

  // VPC specific tags
  

  // IGW specific tags
  igw_tags = {
    "Name" = "igw-${var.aws_region}-${var.vpc_name_gitlab}"
  }

  public_subnets        = [var.cidr_block_dmz_subnet_a, var.cidr_block_dmz_subnet_b]
  public_subnet_prefix  = "sbn"
  public_subnet_suffix  = "${var.env_vpc_gitlab}-dmz"
  rt_public_subnet_name = var.nwk_routing_tables_name["dmz"]

  // DMZ subnet specigic tags
  public_subnet_tags = {
    subnet_type                    = "dmz"
  }

  private_subnets        = [var.cidr_block_private_subnet_a, var.cidr_block_private_subnet_b]
  private_subnet_prefix  = "sbn"
  private_subnet_suffix  = "${var.env_vpc_gitlab}-application"
  rt_private_subnet_name = var.nwk_routing_tables_name["application"]

  // Application subnet specigic tags
  private_subnet_tags = {
    subnet_type                       = "application"
  }

  # Used for application subnets (not only for databases)
  database_subnets             = [var.cidr_block_application_subnet_a, var.cidr_block_application_subnet_b]
  create_database_subnet_group = false # since we use the module for creating private application subnets that are not necessarely meant to database, this parameter will be desactivated
  database_subnet_prefix       = "sbn"
  database_subnet_suffix       = "${var.env_vpc_gitlab}-database"
  rt_database_subnet_name      = var.nwk_routing_tables_name["database"]

  // Database subnet specigic tags
  database_subnet_tags = {
    subnet_type                       = "database"
  }

  enable_nat_gateway = false
  single_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  amazon_side_asn = var.shared_vpc_side_asn
}

output "vpc_module" {
  value = module.vpc
}

locals {
  all_gitlab_subnets = concat(module.vpc.public_route_table_ids, module.vpc.private_route_table_ids, module.vpc.database_route_table_ids)
}

/* Create a S3 endpoint */
module "s3_endpoint" {
  source          = "../modules/s3-endpoint"
  vpc_id          = module.vpc.vpc_id
  route_table_ids = local.all_gitlab_subnets

  // reuse default tags
  tags = merge(
    var.default_tags,
    {
      "Name"        = "s3-endpoint-us-east-1-gitlab"
      "Application" = "none"
      "Role"        = "storage"
      "Environment" = "gitlab"
    }
  )
}



// Fetch dmz routing table id
data "aws_route_table" "rt-dmz" {
  vpc_id = module.vpc.vpc_id
  filter {
    name   = "tag:Name"
    values = [var.nwk_routing_tables_name["dmz"]]
  }
}


