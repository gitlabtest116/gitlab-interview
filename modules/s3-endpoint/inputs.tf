variable "vpc_id" {
  type = string
}

variable "route_table_ids" {
  type = list
}

variable "tags" {
  type = map
}