/* Create an S3 endpoint */
resource "aws_vpc_endpoint" "s3" {
  vpc_id       = var.vpc_id
  service_name = "com.amazonaws.us-east-1.s3"
  tags         = var.tags
}

/* Associate it to the VPC RT */
resource "aws_vpc_endpoint_route_table_association" "s3" {
  count           = length(var.route_table_ids)
  route_table_id  = var.route_table_ids[count.index]
  vpc_endpoint_id = aws_vpc_endpoint.s3.id
}
