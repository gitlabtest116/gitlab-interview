resource "aws_vpc" "custom_vpc" {
  cidr_block              = var.cidr_block
  enable_dns_hostnames    = var.enable_dns_hostnames
  enable_dns_support      = var.enable_dns_support
  instance_tenancy        = "default"
  tags                    = var.tags 
}