variable "cidr_block" {
    type = string
}

variable "enable_dns_hostnames" {
    type = string
}

variable "enable_dns_support" {
    type = string
}

variable "vpc_name" {
    type = string
}

variable "tags" {
    type = map
}