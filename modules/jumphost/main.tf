// Create Security group for gitlab Jump   EC2
resource "aws_security_group" "sg-gitlab-test-jmp" {
  name        = join("-", ["sgrp", var.aws_region, "gitlab-jmp", var.environment])
  description = var.sg_description
  vpc_id      = var.vpc_name



  tags = merge(
    var.default_tags,
    {
      "Name" = join("-", ["sg", var.aws_region, "gitlab-jmp", var.environment])
    }
  )

}



#Jump host Role
resource "aws_iam_role" "iam_jmp_role" {
  name        = "iam-jmp-role-centogen-${var.aws_region}-${var.environment}"
  description = "This role is used for Jump host for gitlab Site"

  assume_role_policy = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "ec2.amazonaws.com",
                    "ecs.amazonaws.com",
                    "ssm.amazonaws.com"
                ]
            },
            "Action": "sts:AssumeRole"
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::667120830894:user/mediatis_ecr"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF
}

# IAM JMP INSTANCE PROFILE

resource "aws_iam_instance_profile" "jmp_profile" {
  name = "jmp-profile-gitlab-${var.aws_region}-${var.environment}"
  role = aws_iam_role.iam_jmp_role.name
}
# IAM JMP EC2 INSTANCE PROFILE POLICY

resource "aws_iam_role_policy" "iam_jmp_profile_policy" {
  name = "iam-jmp-policy-gitlab-${var.aws_region}-${var.environment}"
  role = aws_iam_role.iam_jmp_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Effect": "Allow",
        "Resource": "*",
        "Action": [
            "iam:GetSSHPublicKey",
            "iam:ListSSHPublicKeys",
            "iam:GetUser",
            "iam:GetGroup",
            "iam:ListGroups",
            "iam:ListGroupsForUser",
            "iam:GetRole",
            "sts:GetCallerIdentity",
            "ssm:DescribeAssociation",
            "ssm:GetDeployablePatchSnapshotForInstance",
            "ssm:GetDocument",
            "ssm:GetManifest",
            "ssm:GetParameters",
            "ssm:ListAssociations",
            "ssm:ListInstanceAssociations",
            "ssm:PutInventory",
            "ssm:PutConfigurePackageResult",
            "ssm:UpdateAssociationStatus",
            "ssm:UpdateInstanceAssociationStatus",
            "ssm:UpdateInstanceInformation",
            "cloudwatch:PutMetricData",
            "ds:CreateComputer",
            "ds:DescribeDirectories",
            "ec2:DescribeInstanceStatus",
            "logs:*",
            "ssm:*",
            "ec2messages:*",
            "s3:GetObject",
            "s3:GetObjectVersion",
            "s3:GetBucketVersioning",
            "s3:ListBucket",
	    "ecs:DescribeClusters",
	    "ecs:ListServices",
	    "ecs:UpdateService",
	    "ecs:DescribeServices",
	    "ecs:ListClusters"
        ]
    }]
}
EOF
}

//Create elastic IP for the jump instance
resource "aws_eip" "gitlab-test-eip" {
  vpc = true
}

//Allocating Elastic IP to Jump host
resource "aws_eip_association" "gitlab-test-eip-assoc" {
  instance_id   = aws_instance.gitlab-test-jump-server.id
  allocation_id = aws_eip.gitlab-test-eip.id
}

// Create EC2 instance for gitlab site Jump server
resource "aws_instance" "gitlab-test-jump-server" {
  ami                    = var.instance_details_jmp["ami_id"]
  instance_type          = var.instance_details_jmp["instance_type"]
  subnet_id              = var.dmz_subnet_2
  key_name               = "gitlab_test_jump_key"
  vpc_security_group_ids = [vaws_security_group.sg-gitlab-test-jmp.id]
  root_block_testice {
    volume_size = "50"
    volume_type = "gp2"
    tags = merge(
      var.default_tags_test,
      {
        "Name" = join("-", ["ebs", var.aws_region, "gitlab-jump", var.environment])
      }
    )
  }
  ebs_optimized = true
  user_data     = <<EOF
  #cloud-config
  output : { all : '| tee -a /var/log/cloud-init-output.log' }
  runcmd:
    - zone=`curl http://169.254.169.254/latest/meta-data/placement/availability-zone`
    - instanceId=`curl http://169.254.169.254/latest/meta-data/instance-id`
    - hostname=gitlab-jmp-${var.environment}.${var.network_details["dns_zone"]}
    - echo $hostname > /tmp/hostname
    - hostnamectl set-hostname --static $hostname
    - ip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
  EOF
  tags = merge(
    var.default_tags_test,
    {
      "Name" = join("-", ["ec2", var.aws_region, "gitlab-jump", var.environment])
    }
  )
}

resource "aws_key_pair" "gitlab_test_jump_key" {
  key_name   = "gitlab_test_jump_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC6jcG2i3rH19RfrJmJeRL8SzCJTL4xOpTy8la60YcQjaZuCrv6w0aHdUlMpmtJzCCdQ9C7F+5AtOTKS6tp8jUvaU6ANZafjFAjLlGsuz0IBB9enzXvYpGLy0x0aD9M/E2PqSdw1CFEXaLjbGn6x+4O2hyFLnhyngPIW1J12Lz/gXFj+5P325x+QU+nl7P4nGtXQYq35bGU/GbmUjy8AJJ6RJoMcOmA914NFhsXRf8BKOZ2SNHNr6Rxo7BDHy56OLt0x/yydQFmCa5aViEqmMoDok1c70nhy9d7cz7eKfO9rk2sBNmcNFptc+kun9+UghM+wqUEiCq4e2sEWvXVPQi/rUKJyjjVAanAvSvUvVY/khSd8/7M2niJrY005gtwH+JcC1gj3cKltBqj2f0GmuF0A78PezJo+3hMRaKwCUH5bayDb09Hz+e1fY5hnrds+fFUDDVWAN94e47xO+hQlKJOeDku93NADvRVDEiTbM2h2z+QdlJ0033IjNgoGQ9dN7HdnmcLNB2eOOeO2aPyAK8Xm45u45S2RmdJeQgjBWD68rVcnIObU80jH3YGVunTC2SYHzNi5i80L0z7vzmbByBeUj7W0mKL7xA2EikDZvjLhiF1RAtrtVESgUDOcMu2xwD2XmTp1g7uZ+KiOSY6Sp4Q1uZXguJVqPEx15XyeTMuyQ== jump@gitlab.com"
}


