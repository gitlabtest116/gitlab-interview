# crete kms key for encryption of AWS resources
resource "aws_kms_key" "EncKey" {
  description              = var.kms_description
  deletion_window_in_days  = var.deletion_window_in_days
  key_usage                = var.key_usage
  customer_master_key_spec = var.customer_master_key_spec
  enable_key_rotation      = true # key rotation should happen every year
  is_enabled               = true
  tags                     = var.default_tags
  policy                   = var.policy_enabled == true ? var.policy : ""
}

# adding alias for the key
resource "aws_kms_alias" "alias" {
  name          = var.alias_name
  target_key_id = aws_kms_key.EncKey.key_id
}
