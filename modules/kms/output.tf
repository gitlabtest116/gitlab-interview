output "arn" {
  value = aws_kms_key.EncKey.arn
}

output "id" {
  value = aws_kms_key.EncKey.id
}

output "name" {
  value = aws_kms_alias.alias.name
}
