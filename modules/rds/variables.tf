
variable "sg_description" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "default_tags_dev" {
}

variable "parameters" {
  description = "A list of DB parameter maps to apply"
  type        = list(map(string))
  default     = []
}

variable "family" {
  description = "The family of the DB parameter group"
  type        = string
  default     = "mysql8.0"
}

variable "db_name" {}
#variable "db_identifier" {}
variable "db_instance" {}
variable "db_engine" {}
variable "db_allocated_storage" {}
variable "db_admin" {}
#variable "rds_pass_version" {}
#variable "iops" {}
variable "subnet_db_1" {}
variable "subnet_db_2" {}


variable "deletion_window_in_days" {
  type        = number
  description = "(Optional) Duration in days after which the key is deleted after destruction of the resource, must be between 7 and 30 days. Defaults to 30 days."
  default     = 30
}

variable "kms_description" {
  type = string
}

variable "key_usage" {
  type        = string
  description = "(Optional) Specifies the intended use of the key. Valid values: ENCRYPT_DECRYPT or SIGN_VERIFY. Defaults to ENCRYPT_DECRYPT."
  default     = "ENCRYPT_DECRYPT"
}

variable "customer_master_key_spec" {
  type        = string
  description = "(Optional) Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: SYMMETRIC_DEFAULT, RSA_2048, RSA_3072, RSA_4096, ECC_NIST_P256, ECC_NIST_P384, ECC_NIST_P521, or ECC_SECG_P256K1. Defaults to SYMMETRIC_DEFAULT"
  default     = "SYMMETRIC_DEFAULT"
}


variable "alias_name" {
  type        = string
  description = "(Optional) The display name of the alias. The name must start with the word alias followed by a forward slash (alias/)"
  default     = ""
}

variable "policy_enabled" {
  type        = bool
  description = "Whether policy needs to be enabled or not"
  default     = true
}

variable "policy" {
  type        = string
  description = "A valid policy JSON document. Although this is a key policy, not an IAM policy"
  default     = ""
}

variable "sg_centogene_jmp_dev" {}
variable "sg_centogene_frontend_dev" {}
