//RDS Security Group
resource "aws_security_group" "sgrp-centogene-dev-rds" {
  name        = join("-", ["sgrp", var.aws_region, "centogene-rds", var.environment])
  description = var.sg_description
  vpc_id      = var.vpc_name

  ingress {
    description     = "Allow access from web front-end"
    from_port       = "3306"
    to_port         = "3306"
    protocol        = "tcp"
    security_groups = flatten([var.sg_centogene_frontend_dev])
  }


  ingress {
    description     = "Allow access from web front-end"
    from_port       = "3306"
    to_port         = "3306"
    protocol        = "tcp"
    security_groups = flatten([var.sg_centogene_jmp_dev])
  }


  tags = merge(
    var.default_tags_dev,
    {
      "Name" = join("-", ["sg", var.aws_region, "centogene-rds", var.environment])
    }
  )
}

/*resource "random_string" "db_master_pass" {
  length           = 40
  special          = true
  min_special      = 5
  override_special = "!#$%^&*()-_=+[]{}<>:?"
  keepers = {
    pass_version = var.rds_pass_version
  }
}*/


resource "aws_db_parameter_group" "rds" {
  name_prefix = "${var.db_name}-db-parameter-group"
  description = "DB parameter group for RDS"
  family      = var.family

  dynamic "parameter" {
    for_each = var.parameters
    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = lookup(parameter.value, "apply_method", null)
    }
  }

  tags = merge(
    var.default_tags_dev,
    {
      "Name" = var.db_name
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_instance" "centogene-dev-rds" {
  identifier = var.db_name
  #  name 			      = var.db_name
  parameter_group_name        = aws_db_parameter_group.rds.name
  instance_class              = var.db_instance
  engine                      = var.db_engine
  engine_version              = "8.0.23"
  multi_az                    = false //change this to true in production
  storage_type                = "gp2"
  max_allocated_storage       = 8000
  allocated_storage           = var.db_allocated_storage
  username                    = random_string.user.result
  password                    = random_password.pass.result
  kms_key_id                  = aws_kms_key.EncKey.arn
  apply_immediately           = "true"
  allow_major_version_upgrade = true
  backup_retention_period     = 10
  backup_window               = "09:46-10:16"
  db_subnet_group_name        = aws_db_subnet_group.centogene-dev-rds-db-subnet.name
  vpc_security_group_ids      = [aws_security_group.sgrp-centogene-dev-rds.id]
  publicly_accessible         = false
  skip_final_snapshot         = true
  storage_encrypted           = true

  tags = merge(
    var.default_tags_dev,
    {
      "Name" = join("-", ["rds", var.aws_region, "centogene-rds", var.environment])
    }

  )
}

resource "aws_db_subnet_group" "centogene-dev-rds-db-subnet" {
  tags = merge(
    var.default_tags_dev,
    {
      "Name" = join("-", ["subnet-group", var.aws_region, "centogene-rds", var.environment])
    }
  )
  subnet_ids = [var.subnet_db_1, var.subnet_db_2]
}

/*

resource "aws_security_group_rule" "sgrp-rule-jump-3306" {
  description              = "Allow Traffic from jump instance"
  from_port                = 3306
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sgrp-centogene-dev-rds.id
  to_port                  = 3306
  type                     = "ingress"
  source_security_group_id = var.sg_centogene_jmp_dev
}*/



# crete kms key for encryption of AWS resources
resource "aws_kms_key" "EncKey" {
  description              = var.kms_description
  deletion_window_in_days  = var.deletion_window_in_days
  key_usage                = var.key_usage
  customer_master_key_spec = var.customer_master_key_spec
  enable_key_rotation      = true # key rotation should happen every year
  is_enabled               = true
  tags                     = var.default_tags_dev
  policy                   = var.policy_enabled == true ? var.policy : ""
}

# adding alias for the key
resource "aws_kms_alias" "alias" {
  name          = var.alias_name
  target_key_id = aws_kms_key.EncKey.key_id
}

resource "aws_kms_alias" "storage" {
  name          = "alias/${var.db_name}/rdskey"
  target_key_id = aws_kms_key.EncKey.key_id
}


# use random password generator for password
resource "random_password" "pass" {
  length      = 16
  special     = true
  min_upper   = 1
  min_numeric = 1
}

resource "random_string" "user" {
  length  = 16
  special = false
  lower   = true
  upper   = false
  number  = false
}

# store the admin password for rds in Parameter Store
resource "aws_ssm_parameter" "password" {
  name        = "/${var.db_name}/${var.db_engine}/password"
  description = "Centogene site RDS Admin Password"
  type        = "SecureString"
  value       = random_password.pass.result
  tags        = var.default_tags_dev
  key_id      = aws_kms_alias.storage.name
}

# store the admin username for rds in Parameter Store
resource "aws_ssm_parameter" "username" {
  name        = "/${var.db_name}/${var.db_engine}/username"
  description = "Centogene site RDS Admin Username"
  type        = "SecureString"
  value       = random_string.user.result
  tags        = var.default_tags_dev
  key_id      = aws_kms_alias.storage.name
}

