
variable "sg_web_description" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "default_tags" {
}
variable "az" {
  type = string
}
/*
variable "instance_details_web" {
  type = map(any)
}





variable "network_details" {
  type = map(any)
}

variable "subnet_name_4" {}
variable "subnet_name_5" {}
variable "sg_terminal_server_name" {}
variable "sg_web_lb_dev" {}
variable "ebs_kms_key" {}
variable "host_dns_record" {}*/
