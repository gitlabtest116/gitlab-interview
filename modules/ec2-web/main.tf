// Create Security group for test Jump   EC2
resource "aws_security_group" "sg-test-gitlab" {
  name        = join("-", ["sgrp", var.aws_region, "web", var.environment])
  description = var.sg_web_description
  vpc_id      = var.vpc_name

/*
  egress {
    description     = "Allow SSH forwarding/tunnel to Load Balancer"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = flatten([var.sg_test_lb_gitlab])
  }
  
   egress {
    description     = "Allow outbound internet access to lyso"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
*/
  
  tags = merge(
    var.default_tags,
    {
      "Name" = join("-", ["sg", var.aws_region, "web", var.environment])
    }
  )

}

/*

// Create EC2 instance for test site LysoProof server
resource "aws_instance" "test-gitlab-server" {
  ami                    = var.instance_details["ami_id"]
  instance_type          = var.instance_details["instance_type"]
  subnet_id              = var.subnet_name_4
  key_name               = "test_gitlab_key"
  vpc_security_group_ids = [var.sg_terminal_server_name, aws_security_group.sg-test-gitlab.id]
  root_block_gitlabice {
    volume_size = "50"
    volume_type = "gp2"
    tags = merge(
      var.default_tags_gitlab,
      {
        "Name" = join("-", ["ebs", var.aws_region, "test", var.environment])
      }
    )
  }
  ebs_optimized = true
  user_data     = <<EOF
  #cloud-config
  output : { all : '| tee -a /var/log/cloud-init-output.log' }
  runcmd:
    - zone=`curl http://169.254.169.254/latest/meta-data/placement/availability-zone`
    - instanceId=`curl http://169.254.169.254/latest/meta-data/instance-id`
    - hostname=test-${var.environment}.${var.network_details["dns_zone"]}
    - echo $hostname > /tmp/hostname
    - hostnamectl set-hostname --static $hostname
    - ip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
  EOF
  tags = merge(
    var.default_tags_gitlab,
    {
      "Name" = join("-", ["ec2", var.aws_region, "test", var.environment])
    }
  )
}

resource "aws_key_pair" "test_gitlab_key" {
  key_name   = "test_gitlab_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzVCFS3vK7v0rn0CML788cCWdY0uuYBinuK9pFpk5Wi/ZpADQAJiMXCI08FZi18FWDEaejC7GLNsLn+XLU3lXNHe35HirriwT4Btdz+ssZ4F60wVUOs9iFEaOqZLfC6wy9gXwQSjOlVgCyR8Yxwc3ejTDSvUs9Dzzd1rPgvvxwYLPqfvdrhxDP7klWgl6gxOwzMX6wM5j2wNHexPzyoT3DdGwRlJvehkxgSLCBvYyY+agy9rBdayQmDGG/MDRcNRg7kL2mM0/Dcb34Tc2wbSoUZyQo9yyMnRZfTnDMxyCZAi/KH49sElVhCFRP55Wjp0s3BKKDqAfY7SVObebUrfE8qsE3KlwpiPmRdQAzORyKhEyzHj6liLy0NlD2BjrDJCtdGstEm/zp5e0Ee8nwd3WV4dAYA9TAub5rJPuh3wUTwfMO0XpgTZUcdFYokzBLzTjDl8yxfxP5nRwN3eCrGjW8zlavnB3c+GeoTRwFyy+5wQGpdngv4LkgQ5qbWW+Apo8opfOn2x9ezaa1IyTAxM/WJvSMAOxnlKIjxgLvIjpFE7y/6rKNykOA9VItsiNZyO4Zlaye8EkEuCuE1/K9B9ohj0cWSwndyXHqI/xlV4sXr8CL0s8Zp32OJf9Acfp6/t+NkYR0ArWGcNfK/LNj857NJKTlJruzVXPOpymHUuSoQQ== lyso@test.com"
}

//Creates EBS Volume to Lyso Server

resource "aws_ebs_volume" "test-ebs-gitlab" {
  availability_zone = var.az
  size              = 100
  type              = "gp2"
  tags              = var.default_tags_gitlab
  encrypted         = true
  kms_key_id        = var.ebs_kms_key
}

resource "aws_volume_attachment" "test-gitlab-vol-attach" {
  gitlabice_name  = "/gitlab/xvdb"
  instance_id  = aws_instance.test-gitlab-server.id
  volume_id    = aws_ebs_volume.test-ebs-gitlab.id
  force_detach = true
}



//Create DNS entry to lyso server
resource "aws_route53_record" "test_gitlab_dns" {
  zone_id = var.host_dns_record
  name    = "test-eu-central-1a.gitlab-vpc.aws.test.cloud"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.test-gitlab-server.private_ip]
}


*/