# Security Group allow gitlab site to access the ALB.

resource "aws_security_group" "gitlab_site_lb_sg" {
  name        = join("-", ["sgrp", var.aws_region, "gitlab-alb", var.environment])
  description = var.sg_description
  vpc_id      = var.vpc_name

 /*ingress {
    description     = "Allow Mediatis and gitlab from HTTP"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["62.96.5.68/32","69.210.66.138/31"]
  }

  ingress {
     description     = "Allow Mediatis and gitlab from HTTPS"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks     = ["62.96.5.68/32","69.210.66.138/31"]
  }

  ingress {
    description = "Allow Inbound access on HTTPS"
    from_port   = 443 
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
    description = "Allow Inbound access on HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description     = "Allow HTTP forwarding to web front-ends"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = flatten([var.sg_gitlab_frontend_dev])
  }

  egress {
    description     = "Allow HTTPS forwarding to web front-ends"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = flatten([var.sg_gitlab_frontend_dev])
  }

  egress {
    description     = "Allow HTTPS forwarding to Lysoproof"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = flatten([var.sg_gitlab_lysoproof_dev])
  }*/

  
  tags = merge(
    var.default_tags,
    {
      "Name" = join("-", ["sg", var.aws_region, "gitlab-alb", var.environment])
    }
  )

}



